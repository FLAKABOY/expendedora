var estado = 0;


const producto = document.getElementById("producto");
const btn1Peso = document.getElementById("btn1Peso");
const btn2Pesos = document.getElementById("btn2Pesos");
const btn5Pesos = document.getElementById("btn5Pesos");
const btn10Pesos = document.getElementById("btn10Pesos");
const btn20Pesos = document.getElementById("btn20Pesos");
const btnCambio = document.getElementById("btnCambio");
const btnCancelar = document.getElementById("btnCancelar");
const btnProducto = document.getElementById("btnProducto");
const lblEstado = document.getElementById("lblEstado");
const lblCambio = document.getElementById("lblCambio");

const estado1Peso = {
    0: 1,
    1: 2,
    2: 3,
    3: 4,
    4: 5,
    5: 6,
    6: 7,
    7: 8,
    8: 9,
    9: 10,
    10: 11,
    11: 12,
    12: 13,
    13: 14,
    14: 15,
    15: 16,
    16: 17,
    17: 18,
    18: 19,
    19: 20,
};

const estado2Pesos = {
    0: 2,
    1: 3,
    2: 4,
    3: 5,
    4: 6,
    5: 7,
    6: 8,
    7: 9,
    8: 10,
    9: 11,
    10: 12,
    11: 13,
    12: 14,
    13: 15,
    14: 16,
    15: 17,
    16: 18,
    17: 19,
    18: 20,
};

const estado5Pesos = {
    0: 5,
    1: 6,
    2: 7,
    3: 8,
    4: 9,
    5: 10,
    6: 11,
    7: 12,
    8: 13,
    9: 14,
    10: 15,
    11: 16,
    12: 17,
    13: 18,
    14: 19,
    15: 20,
};

const estado10Pesos = {
    0: 10,
    1: 11,
    2: 12,
    3: 13,
    4: 14,
    5: 15,
    6: 16,
    7: 17,
    8: 18,
    9: 19,
    10: 20,
}

const estado20Pesos = {
    0: 20,
    1: 21,
    2: 22,
    3: 23,
    4: 24,
    5: 25,
    6: 26,
    7: 27,
    8: 28,
    9: 29,
    10: 30,
    11: 31,
    12: 32,
    13: 33,
    14: 34,
    15: 35,
    16: 36,
    17: 37,
    18: 38,
    19: 39,
    20: 40,
}

const desabilitarBotones = () => {
    btn1Peso.disabled = true;
    btn2Pesos.disabled = true;
    btn5Pesos.disabled = true;
    btn10Pesos.disabled = true;
    btn20Pesos.disabled = true;
    btnCancelar.disabled = true;
    btn1Peso.style.textDecoration = `line-through`;
    btn2Pesos.style.textDecoration = `line-through`;
    btn5Pesos.style.textDecoration = `line-through`;
    btn10Pesos.style.textDecoration = `line-through`;
    btn20Pesos.style.textDecoration = `line-through`;
    btnCancelar.style.textDecoration = `line-through`;
};

const habilitarBotones = () => {
    btn1Peso.disabled = false;
    btn2Pesos.disabled = false;
    btn5Pesos.disabled = false;
    btn10Pesos.disabled = false;
    btn20Pesos.disabled = false;
    btnCancelar.disabled = false;
    btn1Peso.style.textDecoration = `none`;
    btn2Pesos.style.textDecoration = `none`;
    btn5Pesos.style.textDecoration = `none`;
    btn10Pesos.style.textDecoration = `none`;
    btn20Pesos.style.textDecoration = `none`;
    btnCancelar.style.textDecoration = `none`;
};

const esFinal = (e) => {
    if (estado >= 18 && estado <= 40){
        e.preventDefault();

        const sonido = new Audio("../media/producto.mp3");

        sonido.addEventListener("ended", () => {
            producto.style.visibility = 'visible';
            desabilitarBotones();
            lblCambio.innerHTML = estado - 18;
        });

        sonido.play();
    }
    else{
        habilitarBotones();
    }
};

const uno = (e) => {
    desabilitarBotones();
    e.preventDefault();

    const sonido = new Audio("../media/moneda.mp3");

    sonido.addEventListener("ended",() => {
        estado = estado1Peso[estado];
        lblEstado.innerHTML = estado;
        esFinal(e);
    });
    sonido.play();
};

const dos = (e) => {
    desabilitarBotones();
    e.preventDefault();

    const sonido = new Audio("../media/moneda.mp3");

    sonido.addEventListener("ended", () => {
        estado = estado2Pesos[estado];
        lblEstado.innerHTML = estado;
        esFinal(e);
    });
    sonido.play();
};

const cinco = (e) => {
    desabilitarBotones();
    e.preventDefault();

    const sonido = new Audio("../media/moneda.mp3");

    sonido.addEventListener("ended", () => {
        estado = estado5Pesos[estado];
        lblEstado.innerHTML = estado;
        esFinal(e);
    });
    sonido.play();
};

const diez = (e) => {
    desabilitarBotones();
    e.preventDefault();

    const sonido = new Audio("../media/moneda.mp3");

    sonido.addEventListener("ended", () => {
        estado = estado10Pesos[estado];
        lblEstado.innerHTML = estado;
        esFinal(e);
    });
    sonido.play();
};

const veinte = (e) => {
    desabilitarBotones();
    e.preventDefault();

    const sonido = new Audio("../media/moneda.mp3");

    sonido.addEventListener("ended", () => {
        estado = estado20Pesos[estado];
        lblEstado.innerHTML = estado;
        esFinal(e);
        });
        sonido.play();
};

const obtenerCambio = (e) => {
    e.preventDefault();

    const sonido = new Audio("../media/cambio.mp3");

    sonido.addEventListener("ended", () => {
        lblCambio.value=0;
        btnCambio.disabled = false;
    });
    if(lblCambio.innerHTML > 0){
        btnCambio.disabled = true;
        sonido.play;
        alert('Ya has obtenido el cambio');
    }
};

const cancelar = (e) => {
    if(estado > 0 && estado <= 18){
        lblCambio.innerHTML = estado;
        estado = 0;
        lblEstado.innerHTML = estado;
        obtenerCambio(e);
    }
};

const obtenerProducto = () => {
    if(producto.style.visibility == 'visible'){
        estado = 0;
        lblEstado.innerHTML = estado;
        producto.style.visibility = 'hiden';
        habilitarBotones();
    }
};

btn1Peso.addEventListener("click",uno);
btn2Pesos.addEventListener("click",dos);
btn5Pesos.addEventListener("click",cinco);
btn10Pesos.addEventListener("click",diez);
btn20Pesos.addEventListener("click",veinte);
btnCambio.addEventListener("click",obtenerCambio);
btnCancelar.addEventListener("click",cancelar);
btnProducto.addEventListener("click",obtenerProducto);